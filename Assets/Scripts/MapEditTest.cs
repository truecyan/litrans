﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine.Events;
public class MapEditTest : EditorWindow
{
    public List<GameObject> TileSet = new List<GameObject>();
    [MenuItem("WindowTesting/Test")]
    static void Open()
    {
        GetWindow<MapEditTest>();

    }


    AnimFloat animFloat = new AnimFloat(0.0001f);
    Texture tex;
    private void OnGUI()
    {

        EditorGUI.BeginChangeCheck();

        Display();
        if (EditorGUI.EndChangeCheck())
        {
            Display();
        }



    }
    void Display()
    {


        var options = new[] { GUILayout.Width(128), GUILayout.Height(128) };
        tex = EditorGUILayout.ObjectField(tex, typeof(Texture), false, options) as Texture;

        GUILayout.Button("Button");
        for (int i = 0; i < TileSet.Count; i++)
        {
            EditorGUILayout.ObjectField(TileSet[i], typeof(GameObject), true);

        }
    }
}