﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapListScript : ScriptableObject
{
    public List<GameObject> TileSet = new List<GameObject>();
    public float xsize;
    public float ysize;
}
