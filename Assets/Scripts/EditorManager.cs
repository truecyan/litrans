﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorManager : MonoBehaviour
{
    private static EditorManager _instance = null;

    public static EditorManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<EditorManager>();
            }
            return _instance;
        }
    }

    public int Width = 32;
    public int Height = 18;



    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
