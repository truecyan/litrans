﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool OnStage;
    public StageManager CurrStage;
    public string PrevStage;
    public float MoveSpeed = 3f;
    public float JumpForce = 10f;
    public float GridWidth = 1f;
    public int MaxReflection = 3;
    public GameObject Crosshair;

    public int CommonLayer;
    public int LightLayer;
    public int RobotLayer;

    public float AirIndex = 1;

    public Sprite RobotOnSprite;
    public Sprite RobotOffSprite;

    private static GameManager _instance = null;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
                if (_instance == null)
                {
                    var Object = Instantiate(Resources.Load<GameObject>("_GameManager/GameManager"));
                    _instance = Object.GetComponent<GameManager>();
                }
            }

            return _instance;
        }
    }

    public float RefractionIndex(string str)
    {
        switch(str)
        {
            case "Glass":
                return 1.5f;
            case "Water":
                return 4f/3f;
            default:
                return 1;
        }
    }

    public void LoadStage(StageManager newStage)
    {
        CurrStage = newStage;
    }

    // Use this for initialization
    void Awake ()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
        CommonLayer = LayerMask.GetMask("Floor") | LayerMask.GetMask("Mirror") | LayerMask.GetMask("Robot") | LayerMask.GetMask("Glass");
        LightLayer = CommonLayer;
        RobotLayer = CommonLayer;

        DontDestroyOnLoad(gameObject);
    }
    // Update is called once per frame
    void Update () {

    }
}

