﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;

public class RobotManager : MonoBehaviour
{
    //public Robot Robot;
    public Dictionary<StageManager.ColorCode, Robot> Robot = new Dictionary<StageManager.ColorCode, Robot>();
    public float AirMovingMax = 4;

    public bool StopMove;

    private StageManager _st;
    private float _origin;
    private ColCheck _feet;
    private LandingCalibrator _land;
    public float MoveSpeed;
    public float JumpForce;
    public float GridWidth;

    public delegate void MovementEvent(bool red, bool green, bool blue);
    public event MovementEvent Move;

    public delegate void CalibrateEvent(bool red, bool green, bool blue);
    public event CalibrateEvent Calibrate;

    // Start is called before the first frame update
    void Start()
    {
        _st = gameObject.GetComponent<StageManager>();
        foreach (StageManager.ColorCode code in Enum.GetValues(typeof(StageManager.ColorCode)))
        {
            Robot[code] = _st.Controlling[code].GetComponent<Robot>();
        }
        MoveSpeed = GameManager.Instance.MoveSpeed;
        GridWidth = GameManager.Instance.GridWidth;
        JumpForce = GameManager.Instance.JumpForce;
        foreach (StageManager.ColorCode code in Enum.GetValues(typeof(StageManager.ColorCode)))
        {
            InitializeStatus(code);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (StopMove) return;
        var status = new Dictionary<StageManager.ColorCode, bool>();
        //오브젝트별로 move 함수 한번씩만 불리도록 수정하기
        var red = false;
        var green = false;
        var blue = false;
        if (Input.GetButton("Red"))
        {
            red = true;
        }
        if (Input.GetButton("Green"))
        {
            green = true;
        }
        if (Input.GetButton("Blue"))
        {
            blue = true;
        }
        if(red||green||blue) Move?.Invoke(red,green,blue);
        if(!red||!green||!blue) Calibrate?.Invoke(red,green,blue);
    }

    public void InitializeStatus(StageManager.ColorCode code)
    {
        Robot[code] = _st.Controlling[code].GetComponent<Robot>();
        Robot[code].Movable = true;
        Robot[code].ColorStatus[code] = true;
        Robot[code].UpdateProperty();
    }
    public void UpdateStatus(StageManager.ColorCode code)
    {
        Robot[code].Movable = false;
        Robot[code].ColorStatus[code] = false;
        Robot[code].UpdateProperty();
        InitializeStatus(code);
    }

    public void Stop()
    {
        StopMove = true;
        foreach (StageManager.ColorCode code in Enum.GetValues(typeof(StageManager.ColorCode)))
        {
            if (Robot[code].IsMoving || !Robot[code].IsLanding)
            {
                Calibrate?.Invoke(false,false,false);
                return;
            }
        }
        _st.LightOn();
    }
    public void OnStopChecked()
    {
        if (!StopMove) return;
        foreach (StageManager.ColorCode code in Enum.GetValues(typeof(StageManager.ColorCode)))
        {
            if (Robot[code].IsMoving || !Robot[code].IsLanding) return;
        }
        _st.LightOn();
    }
    public void Unstop()
    {
        StopMove = false;
    }
}
