﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.VersionControl;
using UnityEngine;
using UnityEngine.UIElements;

public class Robot : MonoBehaviour
{
    public Rigidbody2D Rb2D;

    private float _destX;
    private float _moveSpeed;
    private float _gridWidth;
    private ColCheck _feet;
    private LandingCalibrator _land;
    private SpriteRenderer _sprite;
    private RobotManager _rm;

    public Dictionary<StageManager.ColorCode, bool> ColorStatus = new Dictionary<StageManager.ColorCode, bool>();
    public bool IsMoving;
    public bool Movable;
    public bool Calibrating;
    public bool IsJumping = false;
    public bool IsLanding;
    public bool OnRobot;
    public float Origin;

    // Start is called before the first frame update
    void Awake()
    {
        
    }

    void Start()
    {
        Rb2D = gameObject.GetComponent<Rigidbody2D>();
        _feet = GetComponentInChildren<ColCheck>();
        _land = GetComponent<LandingCalibrator>();
        _sprite = GetComponentInChildren<SpriteRenderer>();
        _moveSpeed = GameManager.Instance.MoveSpeed;
        _gridWidth = GameManager.Instance.GridWidth;
        _rm = GameManager.Instance.CurrStage.RM;
        foreach (StageManager.ColorCode code in Enum.GetValues(typeof(StageManager.ColorCode)))
            ColorStatus[code] = false;
        UpdateProperty();
        _rm.Move += Move;
        _rm.Calibrate += Calibrate;
    }
    private void OnCollisionStay2D(Collision2D other)
    {
        if (!other.gameObject.CompareTag("Robot")) return;
        OnRobot = true;
        var oRobot = other.gameObject.GetComponent<Robot>();
        if (!oRobot.IsLanding) return;
        Rb2D.bodyType = RigidbodyType2D.Dynamic;
    }
    private void OnCollisionExit2D(Collision2D other)
    {
        if (!other.gameObject.CompareTag("Robot")) return;
        OnRobot = false;
        Calibrating = true;
        if (!Movable)
        {
            Rb2D.bodyType = RigidbodyType2D.Dynamic;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (_feet.IsColliding() && Mathf.Abs(Rb2D.velocity.y) <= 0.5f)
        {
            bool check = !IsLanding && !IsMoving;
            IsLanding = true;
            IsJumping = false;
            if(check) _rm.OnStopChecked();
        }
        else IsLanding = false;

        if (!IsLanding&&!IsJumping)
        {
            IsJumping = true;
            SetOrigin();
        }

        if (Calibrating)
        {
            _destX = PosRound(transform.position.x);
            if (_destX < transform.position.x - _gridWidth*_moveSpeed*Time.deltaTime)
            {
                Rb2D.velocity = new Vector2(-_gridWidth*_moveSpeed,Rb2D.velocity.y);
            }
            else if(_destX > transform.position.x + _gridWidth*_moveSpeed*Time.deltaTime)
            {
                Rb2D.velocity = new Vector2(_gridWidth*_moveSpeed,Rb2D.velocity.y);
            }
            else
            {
                EndMove();
            }
        }
    }
    private float HorizontalMove(int sign)
    {
        if (IsJumping && Mathf.Abs(Origin - (transform.position.x + Rb2D.velocity.x * Time.deltaTime)) >= _rm.AirMovingMax)
        {
            if (IsMoving && !Calibrating)
            {
                Calibrating = true;
            }
            return 0;
        }
        Calibrating = false;
        IsMoving = true;
        return sign*_gridWidth * _moveSpeed;
    }
    public void Move(bool red, bool green, bool blue)
    {
        if (!Movable) return;
        float xVel = 0f;
        if (red && ColorStatus[StageManager.ColorCode.Red])
        {
            //Left Move
            xVel += HorizontalMove(-1);
        }
        if (green && ColorStatus[StageManager.ColorCode.Green])
        {
            //Jump
            if (IsLanding)
            {
                Debug.Log("Jump");
                Rb2D.AddForce(Vector2.up * _rm.JumpForce, ForceMode2D.Impulse);
                IsLanding = false;
                IsJumping = true;
                SetOrigin();
            }
        }
        if (blue && ColorStatus[StageManager.ColorCode.Blue])
        {
            //Right Move
            xVel += HorizontalMove(1);
        }
        Rb2D.velocity = new Vector2(xVel, Rb2D.velocity.y);
    }

    public void Calibrate(bool red, bool green, bool blue)
    {
        if (!Movable
            ||(red && ColorStatus[StageManager.ColorCode.Red])
            || (green && ColorStatus[StageManager.ColorCode.Green])
            || (blue && ColorStatus[StageManager.ColorCode.Blue]))
            return;
        Calibrating = true;
        IsMoving = false;
    }
    public bool HasColor()
    {
        return ColorStatus[StageManager.ColorCode.Red] || ColorStatus[StageManager.ColorCode.Green] ||
               ColorStatus[StageManager.ColorCode.Blue];
    }
    public void UpdateProperty()
    {
        if (!Movable && IsLanding && !OnRobot)
            Rb2D.bodyType = RigidbodyType2D.Static;
        else
            Rb2D.bodyType = RigidbodyType2D.Dynamic;
        if (HasColor()) {
            _sprite.sprite = GameManager.Instance.RobotOnSprite;
            //로봇 코어 색 이미지 설정하기
            if (ColorStatus[StageManager.ColorCode.Red])
            {

            }
            if (ColorStatus[StageManager.ColorCode.Green])
            {

            }
            if (ColorStatus[StageManager.ColorCode.Blue])
            {

            }
        }
        else
        {
            _sprite.sprite = GameManager.Instance.RobotOffSprite;
        }
    }
    public void EndMove()
    {
        Rb2D.velocity = new Vector2(0,Rb2D.velocity.y);
        var pos = transform.position;
        transform.position = new Vector3(_destX,pos.y,pos.z);
        Calibrating = false;
        IsMoving = false;
        if (!Movable && IsLanding && !OnRobot)
        {
            Rb2D.bodyType = RigidbodyType2D.Static;
        }
        GameManager.Instance.CurrStage.RM.OnStopChecked();
    }
    public void SetOrigin()
    {
        if (Rb2D.velocity.x < 0)
            Origin = PosCeil(transform.position.x);
        else
            Origin = PosFloor(transform.position.x);
    }

    private float PosRound(float pos)
    {
        return Mathf.Round(pos+0.5f)-0.5f;
    }
    private float PosCeil(float pos)
    {
        return Mathf.Ceil(pos + 0.5f) - 0.5f;
    }
    private float PosFloor(float pos)
    {
        return Mathf.Floor(pos + 0.5f) - 0.5f;
    }
}
