﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectData : MonoBehaviour
{

    public enum Category
    {
        Controlling,
        Block,
        Special
    }

    public enum Type
    {
        Floor,
        Mirror,
        Glass,
        Robot,
        Mixed
    }

    public Category ObjCat;
    public Vector2 Position;
    public bool Movable;
    public bool IsMain;


    // Start is called before the first frame update
    void Start()
    {
        if (ObjCat != Category.Controlling && IsMain) IsMain = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string GetDataString()
    {
        string str = "";



        return str;
    }
}
