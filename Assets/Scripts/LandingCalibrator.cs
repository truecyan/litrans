﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandingCalibrator : MonoBehaviour
{
    private Rigidbody2D rb2D;

    private float prevPos;
    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 pos = transform.position;

        if (rb2D.velocity.y > 0) return;

        CheckHor();

        var downRay = Physics2D.Raycast(pos - new Vector2(0, 1.5f), Vector2.down, -rb2D.velocity.y * Time.fixedDeltaTime, GameManager.Instance.RobotLayer);

        Debug.DrawRay(pos - new Vector2(0, 1.5f), Vector2.down * -rb2D.velocity.y * Time.fixedDeltaTime);
        if (downRay)
        {
            //Debug.Log(downRay.transform.gameObject);
            //Debug.Log(downRay.transform);
            //Debug.Log(downRay.distance);

            transform.position -= new Vector3(0,downRay.distance);
            rb2D.velocity = new Vector2(rb2D.velocity.x,0);
        }
    }

    public void CheckHor()
    {
        Vector2 pos = transform.position;

        if (rb2D.velocity.x > 0)
        {
            var frontRay = Physics2D.Raycast(pos + new Vector2(0.475f, -1.49f), Vector2.right, rb2D.velocity.x * Time.fixedDeltaTime, GameManager.Instance.RobotLayer);
            if (frontRay)
            {
                if (frontRay.transform.CompareTag("Robot")) return;
                //Debug.Log(frontRay.transform);
                //Debug.DrawRay(pos + new Vector2(0.475f, -1.5f), Vector2.right);
                transform.position += new Vector3(frontRay.distance-0.01f,0);
                rb2D.velocity = new Vector2(0,rb2D.velocity.y);
            }
            else
            {
                frontRay = Physics2D.Raycast(pos + new Vector2(0.475f, -0.5f), Vector2.right, rb2D.velocity.x * Time.fixedDeltaTime, GameManager.Instance.RobotLayer);
                if (frontRay)
                {
                    Debug.Log("OK");
                    if (frontRay.transform.CompareTag("Robot")) return;
                    transform.position += new Vector3(frontRay.distance-0.01f,0);
                    rb2D.velocity = new Vector2(0,rb2D.velocity.y);
                }
            }
        }
        else if (rb2D.velocity.x < 0)
        {
            var frontRay = Physics2D.Raycast(pos + new Vector2(-0.475f, -1.49f), Vector2.left, -rb2D.velocity.x * Time.fixedDeltaTime, GameManager.Instance.RobotLayer);
            if (frontRay)
            {
                if (frontRay.transform.CompareTag("Robot")) return;
                transform.position -= new Vector3(frontRay.distance-0.01f,0);
                rb2D.velocity = new Vector2(0,rb2D.velocity.y);
            }
            else
            {
                frontRay = Physics2D.Raycast(pos + new Vector2(-0.475f, -0.5f), Vector2.left, -rb2D.velocity.x * Time.fixedDeltaTime, GameManager.Instance.RobotLayer);
                if (frontRay)
                {
                    if (frontRay.transform.CompareTag("Robot")) return;
                    transform.position -= new Vector3(frontRay.distance-0.01f,0);
                    rb2D.velocity = new Vector2(0,rb2D.velocity.y);
                }
            }
        }
    }
}
