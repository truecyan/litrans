﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{
    private Camera _camera;
    private StageManager _stage;
    // Start is called before the first frame update
    void Awake()
    {
        _camera = Camera.main;
        _stage = GameManager.Instance.CurrStage.GetComponent<StageManager>();
    }

    // Update is called once per frame
    void Update()
    {
        PosUpdate();   
    }

    public void PosUpdate()
    {
        Vector3 prev = transform.position;
        Vector2 pos = _camera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 h = new Vector2(PosRound(pos.x), Round(pos.y));
        Vector2 v = new Vector2(Round(pos.x), PosRound(pos.y));
        if ((pos - h).magnitude < (pos - v).magnitude)
            transform.position = new Vector3(h.x, h.y, 0);
        else
            transform.position = new Vector3(v.x, v.y, 0);
        if (prev != transform.position)
        {
            _stage.LightUpdate();
        }
    }
    private float PosRound(float pos)
    {
        return Mathf.Round(pos+0.5f)-0.5f;
    }
    private float Round(float pos)
    {
        return Mathf.Round(pos);
    }
}
