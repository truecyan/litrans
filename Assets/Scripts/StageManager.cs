﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngineInternal;
using Color = UnityEngine.Color;


public class StageManager : MonoBehaviour
{
    //public bool IsRoot;

    public enum ColorCode
    {
        Red,
        Green,
        Blue
    }
    public Dictionary<ColorCode, GameObject> Controlling = new Dictionary<ColorCode, GameObject>();
    public Dictionary<ColorCode, LineRenderer> Lights = new Dictionary<ColorCode, LineRenderer>();
    public Dictionary<ColorCode, List<Vector3>> PointList = new Dictionary<ColorCode, List<Vector3>>();
    public GameObject InitR;
    public GameObject InitG;
    public GameObject InitB;
    public LineRenderer RedLine;
    public LineRenderer GreenLine;
    public LineRenderer BlueLine;
    
    private float _rayMax = 100;
    private GameObject _crosshair;
    //private List<Line> _lineList = new List<Line>();

    [HideInInspector]public RobotManager RM;

    private Dictionary<ColorCode, GameObject> _next = new Dictionary<ColorCode, GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        //인스펙터에서 시작 오브젝트 할당할 때는 딕셔너리 사용 불가
        Lights[ColorCode.Red] = RedLine;
        Lights[ColorCode.Green] = GreenLine;
        Lights[ColorCode.Blue] = BlueLine;

        PointList[ColorCode.Red] = new List<Vector3>();
        PointList[ColorCode.Green] = new List<Vector3>();
        PointList[ColorCode.Blue] = new List<Vector3>();

        Controlling[ColorCode.Red] = InitR;
        Controlling[ColorCode.Green] = InitG;
        Controlling[ColorCode.Blue] = InitB;

        GameManager.Instance.LoadStage(this);

        RM = gameObject.GetComponent<RobotManager>();
    }

    public Color Converter(ColorCode code)
    {
        switch (code)
        {
            case ColorCode.Red:
                return Color.red;
            case ColorCode.Green:
                return Color.green;
            case ColorCode.Blue:
                return Color.blue;
            default:
                return Color.white;
        }
    }

    public Color SetAlpha(Color color, float alpha)
    {
        return new Color(color.r,color.g,color.b,alpha);
    }

    //빛 발사 시작 함수 - 마커 위치 변경될 때만 실행
    public void LightUpdate()
    {
        //RGB원소별로 진행
        foreach (ColorCode code in Enum.GetValues(typeof(ColorCode)))
        {
            Lights[code].gameObject.SetActive(true); //라인렌더러 활성화
            PointList[code].Clear(); //빛 경로 비우기
        }
        foreach (ColorCode code in Enum.GetValues(typeof(ColorCode)))
        {
            
            Vector2 from = Controlling[code].transform.position - new Vector3(0, 0.5f, 0); //로봇 차지 공간 (2x1) 중심 기준
            PointList[code].Add(from); //최초 지점 추가
            Vector2 to = _crosshair.transform.position;
            Lights[code].startColor = SetAlpha(Converter(code), 0.5f);
            Lights[code].endColor = SetAlpha(Converter(code), 0.5f);
            if (from != to) //아주 조금이라도 거리가 있을때만 빛 쏘기 진행.
            {
                var dir = (to - from).normalized;
                var lightRay = Physics2D.Raycast(from, dir, _rayMax, GameManager.Instance.LightLayer);
                //최초의 레이가 닿은 지점 기준으로 분기를 나눔.
                if (lightRay)
                {
                    var point = lightRay.point;
                    var obj = lightRay.transform.gameObject;
                    PointList[code].Add(point);
                    var norm = lightRay.normal;
                    if (obj.CompareTag("Mirror")) //반사
                    {
                        obj = Reflect(dir, point, norm, GameManager.Instance.MaxReflection,
                            GameManager.Instance.AirIndex, null, code);
                    }
                    else if (obj.CompareTag("Glass")) //굴절
                    {
                        var col = lightRay.collider;
                        obj = RefractIn(dir, point, norm, GameManager.Instance.MaxReflection,
                            GameManager.Instance.AirIndex, lightRay.collider, code);
                    }

                    //반사 또는 굴절의 효과를 재귀적으로 체크 후 최종적으로 리턴되는 오브젝트가 로봇일 때
                    if (obj && obj.CompareTag("Robot"))
                    {
                        //로봇에 닿았을 때 빛을 선명하게 변경
                        Lights[code].startColor = SetAlpha(Converter(code), 1f);
                        Lights[code].endColor = SetAlpha(Converter(code), 1f);
                        _next[code] = obj; //컨트롤 변경 대기에 등록
                    }
                    else _next[code] = null; //로봇이 아니면 패스
                }
                else //아무데도 안닿았을 경우 충분히 긴 레이저를 그리도록 함.
                {
                    PointList[code].Add(from + dir * _rayMax);
                    _next[code] = null; 
                }
                
            }
            //재귀함수를 통해 얻은 빛 경로 등록 
            Lights[code].positionCount = PointList[code].Count; //사전에 크기 변경을 안하면 최초 사이즈로 배열이 잘림.
            Lights[code].SetPositions(PointList[code].ToArray());
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Light")) //누를 때
        {
            RM.Stop(); //멈춘게 확인되면 LightOn 함수 실행
        }
        else if (Input.GetButtonUp("Light")) //뗄 때
        {
            foreach (ColorCode code in Enum.GetValues(typeof(ColorCode)))
            {
                Lights[code].gameObject.SetActive(false); //라인렌더러 비활성화
            }
            RM.Unstop(); //이동 잠금 해제
            if(_crosshair)
                _crosshair.SetActive(false); //마커 비활성화

            foreach (ColorCode code in Enum.GetValues(typeof(ColorCode)))
            {
                if (_next[code])
                {
                    Controlling[code] = _next[code]; //컨트롤 변경 적용
                    RM.UpdateStatus(code);
                }
            }
        }
    }

    public void LightOn()
    {
        if (_crosshair) _crosshair.SetActive(true); //마커를 활성화 하는 것으로 빛 발사가 시작됨
        else _crosshair = Instantiate(GameManager.Instance.Crosshair); //처음엔 마커가 없어서 만들어줘야됨
        _crosshair.GetComponent<Crosshair>().PosUpdate(); //이전 위치로 한프레임 튀는거 고치는 용도
        LightUpdate(); //한 지점으로 여러번 쏘는 등 마커 위치가 안바뀔 때를 대비해 한번은 여기서 실행
    }
    private GameObject Reflect(Vector2 inDir, Vector2 point, Vector2 norm, int count, float refIndex, Collider2D medCol, ColorCode code)
    {
        var reflect = Vector2.Reflect(inDir, norm); //반사각 구하기
        if (refIndex > 1) //유리/물 속에 있을 때 발동
        {
            return RefractOut(reflect, point, count, refIndex, medCol, code); //나가는 방향 굴절 계산
        }
        //추가적인 반사/굴절 계산
        var ray = Physics2D.Raycast(point, reflect, _rayMax, GameManager.Instance.LightLayer);
        if (ray)
        {
            PointList[code].Add(ray.point); //경로에 추가
            if (ray.transform.CompareTag("Mirror"))
            {
                //거울 반사는 반사 카운트 소모
                if (count > 1) return Reflect(reflect, ray.point, ray.normal, count - 1, refIndex, medCol, code);
            }
            else if (ray.transform.CompareTag("Glass"))
            {
                return RefractIn(reflect, ray.point, ray.normal, count, refIndex, ray.collider, code);
            }
            return ray.transform.gameObject;
        }
        PointList[code].Add(point + reflect * _rayMax); //아무데도 안닿는 경우
        return null;
    }

    //굴절 벡터만 구해서 RefractOut함수로 넘김
    private GameObject RefractIn(Vector2 inDir, Vector2 point, Vector2 norm, int count, float refIndex, Collider2D selfCol, ColorCode code)
    {
        var n = GameManager.Instance.RefractionIndex(selfCol.gameObject.tag);
        var proj = Vector2.Dot(inDir, -norm)*(-norm);
        var v = (inDir - proj)*(refIndex/n); //굴절 벡터의 법선과 수직인 성분
        var u = Mathf.Sqrt(1 - v.sqrMagnitude) * (-norm); //굴절 벡터의 법선과 평행한 성분
        var refract = u + v; //굴절 벡터
        return RefractOut(refract, point, count, n, selfCol, code);
    }

    //인접 타일은 좌표를 기준으로 계산. 조건: (+-1,0) (0,+-1) (0,0) 
    private bool AdjColCheck(Collider2D col1, Collider2D col2)
    {
        var pos1 = col1.transform.position;
        var pos2 = col2.transform.position;
        var xDiff = Mathf.Abs(pos1.x - pos2.x);
        var yDiff = Mathf.Abs(pos1.y - pos2.y);
        return (xDiff < 0.01 && yDiff > 0.99 && yDiff < 1.01)||(yDiff < 0.01 && xDiff > 0.99 && xDiff < 1.01) || (xDiff<0.01&&yDiff<0.01);
    }

    //나가는 방향 굴절 계산 및 매질 내부 진행 계산 (전반사, 인접 매질 연계)
    private GameObject RefractOut(Vector2 inDir, Vector2 point, int count, float refIndex, Collider2D selfCol, ColorCode code)
    {
        //매질 내부 진행 레이 - 대각선 길이보다 살짝 길게 (1.42)
        var inRay = Physics2D.Raycast(point, inDir, 1.42f, GameManager.Instance.LightLayer);
        if (inRay)
        {
            var nextCol = inRay.collider;
            var nextPoint = inRay.point;
            var norm = inRay.normal;
            while (nextCol == selfCol) //기준 콜라이더에 다시 충돌하는 것 방지
            {
                var temp = inRay.transform.gameObject;
                temp.SetActive(false);//수동 충돌 무시
                inRay = Physics2D.Raycast(point, inDir, 1.42f, GameManager.Instance.LightLayer);
                temp.SetActive(true);
                nextCol = inRay.collider;
                nextPoint = inRay.point;
                norm = inRay.normal;
            }
            if (inRay && AdjColCheck(inRay.collider,selfCol)) //인접한 타일일 경우 --> 공기를 못 만난 경우
            {
                if(point!=nextPoint) //같은 지점 추가 방지
                    PointList[code].Add(nextPoint);
                if (inRay.transform.CompareTag("Glass")) //유리를 만날 경우 진행 방향 유지하며 RefractOut 호출 (콜라이더는 다음 유리로 넘어감)
                {
                    return RefractOut(inDir, nextPoint, count, refIndex, nextCol, code);
                }
                if (inRay.transform.CompareTag("Mirror")) //거울을 만날 경우 반사 호출 (굴절률로 인해 다시 RefractOut 호출 될것)
                {
                    if (count > 1) return Reflect(inDir, nextPoint, norm, count-1, refIndex, selfCol, code);
                }

                return nextCol.gameObject;
            }
            
        }
        //빛이 콜라이더에서 탈출하는 지점을 찾기 위해 반대방향으로 레이캐스트
        var reverseRay = Physics2D.Raycast(point + inDir * 1.42f, -inDir, 1.42f, GameManager.Instance.LightLayer);
        if (reverseRay)
        {
            var n = GameManager.Instance.AirIndex;
            var norm = reverseRay.normal;
            var outPoint = reverseRay.point;
            PointList[code].Add(outPoint);
            var proj = Vector2.Dot(inDir, norm) * (norm);
            var v = (inDir - proj) * (refIndex/n); //굴절 벡터의 법선과 수직인 성분
            if (v.magnitude > 1)
            {        
                return Reflect(inDir, outPoint, -norm, count, refIndex, selfCol, code);//전반사
            }
            var u = Mathf.Sqrt(1 - v.sqrMagnitude) * (norm); //굴절 벡터의 법선과 평행한 성분
            var refract = u + v; //나가는 방향 굴절
            var outRay = Physics2D.Raycast(outPoint, refract, GameManager.Instance.LightLayer);
            //추가적인 반사/굴절 계산
            if (outRay)
            {
                PointList[code].Add(outRay.point);
                if (outRay.transform.CompareTag("Mirror"))
                {
                    if (count > 1) return Reflect(refract, outRay.point, outRay.normal, count - 1, GameManager.Instance.AirIndex, null, code);
                }
                else if (outRay.transform.CompareTag("Glass"))
                {
                    return RefractIn(refract, outRay.point, outRay.normal, count, GameManager.Instance.AirIndex, outRay.collider, code);
                }
                return outRay.transform.gameObject;
            }
            PointList[code].Add(outPoint + refract * _rayMax);
        }
        return null;
    }

    public void StageExit()
    {
        //이전 스테이지로 이동
    }

    public void StageEnter()
    {
        //test box가 가리키는 스테이지로 이동
    }
}